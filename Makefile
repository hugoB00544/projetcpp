# Ceci est mon premier Makefile
#<cible> : <dependances>
#	<commande>

#NOM = VALEUR
CC = g++
EXEC = main
CFLAGS = -Wall -O2 -g -lpthread -lsqlite3
OS = ubuntu

ifeq ($(OS), win)
	SUPP = del 
else
	SUPP = rm -rf
endif


#Tous les fichiers sources .cpp
SRC = $(wildcard *.cpp)

#On fai correspondre à chaque .cpp, un .o
OBJ = $(SRC:.cpp=.o)

all : info $(EXEC)

#Generer  l'executable issu de tous .o générés prcédemment
$(EXEC) : $(OBJ)
	$(CC)  -o $(EXEC) $^ $(CFLAGS)

#Générer autant de .o que de .c dans le repertoire	
%.o : %.cpp
	$(CC) -o $@ -c $< $(CFLAGS)

clean :
#linux
#	rm -rf *.o	 
	$(SUPP) $(OBJ) 2> erreur.log

mrproper : clean
ifeq ($(OS), win)
	$(SUPP) $(EXEC).exe
else
	$(SUPP) $(EXEC)
endif
	$(SUPP) *.log

zip : 
	tar.exe -a -c -f sortie.zip $(SRC) *.h

info : 
ifeq ($(OS), win)
	@echo Windows
else
	@echo Linux
endif

#compression :
#	Compress-Archive -path "C:/Users/Administrateur/Desktop/AKKACPP/DevOpensource/Ex3/outils.c" -destinationpath "C:/Users/Administrateur/Desktop/AKKACPP/DevOpensource/Ex3/fic.zip"

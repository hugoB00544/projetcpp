#ifndef MISSION_H
#define MISSION_H

#include <iostream>
#include <vector>
#include <list>
#include "json.hpp"
#include "posteDePilotage.h"
#include "ctime"
#include "chrono"
#include <sqlite3.h>

using json = nlohmann::json;


enum MissionType { Standby = 0, Stop = 1, RepriseDeMission = 2, DemandeDeRapport = 3, Deminage = 4, Mesure = 5,Retour = 6, Autre = -1 };



class Mission
{
public:
    Mission(MissionType t, const std::string& vtitre, int vid = -1);

    void infos() const;
    int getIdMission() const;
    void addLieu(const std::string& lieu);
    vector<PosteDePilotage*> getPilotages() const;
    void addPilotage(PosteDePilotage* pp);
    std::string getMissionType() const;
    std::string getTitre() const;
    std::string getEtat() const {return etat;};
    std::string getDateDebut() const {return dateDebut;};
    std::string getDateFin() const {return dateFin;};
    void setEtat(std::string e){etat = e;};
    json getJson(PosteDePilotage* pp) const;
    void SendMission();
    void setDateDebut(string date){dateDebut = date;};
    void setDateFin(string date){dateFin = date;};
    vector<string> getLieux()const {return lieux;};
    void SendMissionPilote(PosteDePilotage*);
    static int nextIdMission;
    void setContent(string cont){content = cont;};
    string getContent()const{return content;};
    void updateBddMission();
    void updateBddLieu(string);
    void updateBddPilote(PosteDePilotage*);
    bool estAttribueeA(PosteDePilotage*);
	void setTitre(std::string vtitre){titre = vtitre;};
	void setType(MissionType vtype){type = vtype;};
private:
    int idMission;
    MissionType type;
    std::string titre;
    std::vector<std::string> lieux;
    std::vector<PosteDePilotage*> pilotages;
    string etat;
    string dateDebut;
    string dateFin;
    string content;
};

#endif // MISSION_H


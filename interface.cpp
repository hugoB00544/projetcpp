#include <iostream>
#include "interface.h"
#include <string.h>
using namespace std;


bool is_number(const std::string& s)
{
    return !s.empty() && std::find_if(s.begin(),
        s.end(), [](unsigned char c) { return !std::isdigit(c); }) == s.end();
}

void Demarrage(PosteDeCommandement* pc){

        pc->DemarrageServer();


}

void EnvoiMission(PosteDeCommandement* pc, Mission* m){

        pc->envoyerOrdre(*m);
}

void interfaceStart(){
	system("clear");
	PosteDeCommandement pc;
	pc.initDB();
	thread t(Demarrage,&pc);
	t.detach();
	accueil(&pc);


	return;
}


void accueil(PosteDeCommandement* pc){

	bool retour = false;

	while(retour == false){

	cout << "=========== ACCUEIL ============"<<endl<<endl;
	cout << " 0 : Ajouter un nouveau poste de pilotage." << endl;
	cout << " 1 : Selectionner un poste de pilotage."<<endl;
	cout << " 2 : Envoyer une nouvelle mission." << endl;
	cout << " 3 : Liste des missions."<<endl;
	cout << " 4 : Observer l'avancement de l'operation dans son ensemble." << endl;
	cout << " 5 : Lire les logs." << endl;
	cout << " Q : Quitter l'application." << endl;
	cout << endl;
	char val;
	cout << "Saisir une option : ";
	cin >> val;
	cin.ignore(numeric_limits<streamsize>::max(),'\n');
	cout << endl;
	system("clear");
	switch(val){
		case '0':
			ajouterPosteDePilotage(pc);
			break;
		case '1':
			selectionPilotage(pc);
		       break;
		case '2':
		       afficherMenuNouvelleMission(pc);
		       break;
		case '3':
			afficherChoixTypeListeMissions(pc);
		       break;
		case '4':
		       afficherAvancementOperation();
		       break;
		case '5':
		       afficherLogs();
			break;
		case 'Q':
			retour = true;
			break;
	 	default:
			cout << "L'option saisie n'est pas valide!"<<endl;
			break;

	}

	}


}



void selectionPilotage (PosteDeCommandement* pc){
	
	
	bool retour = false;

	while(retour == false){
		cout << "========== Selectionner un poste de pilotage ==========" << endl;
		cout << "Liste des postes de pilotages."<< endl;
		auto pilotages = pc->getPilotages();
		for(const auto& kv:pilotages){

			cout << " "  << kv.first << " : poste de pilotage : " << kv.second->getName().c_str() << endl;


		}
		cout << " R : Retour. " << endl;
		cout << endl;

		string choix;

		cout << "Saisir une option : ";
		cin >> choix;
	cin.ignore(numeric_limits<streamsize>::max(),'\n');
		cout << endl;
		system("clear");

                        if(is_number(choix) && pilotages.count(stoi(choix)) >0){
				afficherInfoPilotage(pc,pilotages[stoi(choix)]);


			}
			
			else {if(choix == "R"){

					retour = true;
				}else{

					cout << "L'option saisie n'est pas valide!"<<endl;
				}
			}

	
	}
	

	return;


}


void afficherAvancementOperation() {
	bool retour = false;

	while(retour == false){
    std::cout << "======= Avancement de l'operation ========" << std::endl;
	afficherStatistiques();
    //  Afficher l'état d'avancement de l'operation
    std::cout << "R. Retour" << std::endl;
    std::cout << "Saisir une option : ";
    char choix;
    std::cin >> choix;
	cin.ignore(numeric_limits<streamsize>::max(),'\n');
	system("clear");
	switch(choix){
			case 'R':
				retour = true;
				break;
	 	default:
			cout << "L'option saisie n'est pas valide!"<<endl;
			break;
		}

	}	
}



void afficherChoixTypeListeMissions(PosteDeCommandement* pc) {
        bool retour = false;

        while(retour == false){
    std::cout << "======= Listes des missions ========" << std::endl;
	std::cout << " 0 : Listes des missions terminees"<<endl;
	cout<<" 1 : Listes des missions en cours"<<endl;
    std::cout << "R. Retour" << std::endl;
    std::cout << "Saisir une option : ";
    char choix;
    std::cin >> choix;
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        system("clear");
        switch(choix){
		case '0':
			afficherSelectionTermineeMission(pc);
			break;
		case '1':
			afficherSelectionModifMission(pc);
			break;
                case 'R':
                        retour = true;
                        break;
                default:
                        cout << "L'option saisie n'est pas valide!"<<endl;
                        break;
                }

        }
}



void afficherLogs(){

	bool retour = false;

	while(retour == false){
    std::cout << "======== 10 derniers Logs ========" << std::endl;
    
	ifstream myfile("logs/logs.txt");
   string line, buffer[10];
   const size_t size = sizeof buffer / sizeof *buffer;
   size_t i = 0;
   /*
    * Read all lines of file, putting them into
    * a circular buffer of strings.
    */
   while ( getline(myfile, line) )
   {
      buffer[i] = line;
      if ( ++i >= size )
      {
         i = 0;
      }
   }
   /*
    * Print the lines.
    */
   for ( size_t j = 0; j < size; ++j )
   {
      cout << buffer[i] << "\n";
      if ( ++i >= size )
      {
         i = 0;
      }
   }



    std::cout << "R. Retour" << std::endl;
    std::cout << "Saisir une option : ";
    char choix;
    std::cin >> choix;
	cin.ignore(numeric_limits<streamsize>::max(),'\n');
	system("clear");	
	switch(choix){
			case 'R':
				retour = true;
				break;
	 	default:
			cout << "L'option saisie n'est pas valide!"<<endl;
			break;
		}

	}	


}



void ajouterPosteDePilotage(PosteDeCommandement* pc){


    std::string ip;
    int port;
    std::string type;
    std::string nom;

    std::cout << "=== Ajouter un nouveau poste de pilotage ===" << std::endl;
    std::cout << "Saisir IP : ";
    std::cin >> ip;
	cin.ignore(numeric_limits<streamsize>::max(),'\n');
    std::cout << "Saisir port : ";
    std::cin >> port;
	cin.ignore(numeric_limits<streamsize>::max(),'\n');
    std::cout << "Saisir type : ";
    std::getline(cin, type);
    
    std::cout << "Saisir nom : ";
    std::getline(cin, nom);
	
    system("clear");

    PosteDePilotage* pp = new PosteDePilotage(ip, port, nom, type);
    pc->ajoutPilotage(pp);



	sqlite3* db= initialiserConnexion();
	std::string query = "INSERT INTO PosteDePilotage (idPilotage, ip,port,name,type,idCommandement) VALUES (?, ?, ?, ?, ?,1);";
    sqlite3_stmt* stmt;

    int rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de la préparation de la requête 1 : " << sqlite3_errmsg(db) << std::endl;
        return;
    }
    sqlite3_bind_int(stmt, 1, pp->getId());
    sqlite3_bind_text(stmt, 2, pp->getIp().c_str(), strlen(pp->getIp().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_int(stmt, 3, pp->getPort());
    sqlite3_bind_text(stmt, 4, pp->getName().c_str(), strlen(pp->getName().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 5, pp->getType().c_str(), strlen(pp->getType().c_str()), SQLITE_TRANSIENT);

    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE)
    {
        std::cerr << "Erreur lors de l'exécution de la requête 1 : " << sqlite3_errmsg(db) << std::endl;
    }

    sqlite3_finalize(stmt);



sqlite3_close(db);

    ajouterRover(pp);
}

void ajouterRover(PosteDePilotage* pp){


    std::string ip;
    int port;
    std::string type;
    std::string nom;
    string stream;

    std::cout << "=== Ajouter un rover au poste de pilotage ===" << std::endl;
    std::cout << "Saisir IP : ";
    std::cin >> ip;
	cin.ignore(numeric_limits<streamsize>::max(),'\n');
    std::cout << "Saisir port : ";
    std::cin >> port;
	cin.ignore(numeric_limits<streamsize>::max(),'\n');

    	std::cout << "Saisir type : ";
    std::getline(cin, type);

    std::cout << "Saisir nom : ";
    std::getline(cin, nom);

	std::cout << "Saisir l'URL du stream : ";
    std::getline(cin, stream);

    system("clear");

    Rover* rover = new Rover(ip, port, nom, type);
	rover->setEtat("Disponible");
	rover->setStream(stream);
    pp->setRover(rover);


    sqlite3* db= initialiserConnexion();
        std::string query = "INSERT INTO Rover (idRover, ip,port,name,type,idPilotage,etat) VALUES (?, ?, ?, ?, ?,?,?);";
    sqlite3_stmt* stmt;

    int rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de la préparation de la requête 1 : " << sqlite3_errmsg(db) << std::endl;
        return;
    }
    sqlite3_bind_int(stmt, 1, rover->getId());
    sqlite3_bind_text(stmt, 2, rover->getIp().c_str(), strlen(rover->getIp().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_int(stmt, 3, rover->getPort());
    sqlite3_bind_text(stmt, 4, rover->getName().c_str(), strlen(rover->getName().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 5, rover->getType().c_str(), strlen(rover->getType().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_int(stmt, 6,pp->getId());
    sqlite3_bind_text(stmt, 7, rover->getStream().c_str(), strlen(rover->getStream().c_str()), SQLITE_TRANSIENT);


    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE)
    {
        std::cerr << "Erreur lors de l'exécution de la requête 1 : " << sqlite3_errmsg(db) << std::endl;
    }

    sqlite3_finalize(stmt);



sqlite3_close(db);

}


void afficherInfoPilotage(PosteDeCommandement* pc, PosteDePilotage* pp){

	
	bool retour = false;

	while(retour == false){
    
    std::cout << "=== Informations du poste de pilotage ===" << std::endl;
    std::cout << "Nom : " << pp->getName().c_str() << std::endl;
    std::cout << "Type : " << pp->getType().c_str() << std::endl;
    std::cout << "IP : " << pp->getIp().c_str() << std::endl;
    std::cout << "Port : " << pp->getPort() << std::endl<<endl;
    std::cout << " 1 : Ouvrir la video du rover." << endl;
    std::cout << " 2 : Liste des missions envoyee au poste de pilotage." << endl;
    std::cout << "R. Retour" << std::endl;
    std::cout << "Saisir une option : ";
    char choix;
    std::cin >> choix;
	cin.ignore(numeric_limits<streamsize>::max(),'\n');
	system("clear");
	switch(choix){
			case '1':
				ouvrirStream(pp);
				break;
			case '2':
				afficherListeMission(pc,pp);
				break;
			case 'R':
				retour = true;
				break;
	 	default:
			cout << "L'option saisie n'est pas valide!"<<endl;
			break;
		}

	}	

}



void afficherListeMission(PosteDeCommandement* pc, PosteDePilotage* pp){

	bool retour = false;

	while(retour == false){
    std::cout << "======== Liste des Missions envoyees ========" << std::endl;
    

	auto missions = pc->getMissions();
	int i = 0;
	for(auto mission : missions){
		if(mission->estAttribueeA(pp)){
			cout<< " "<<i<<" : titre -> " << mission->getTitre().c_str() << ", etat -> "<<mission->getEtat().c_str()<<endl;
			i++;
		}

	}

    std::cout << "R. Retour" << std::endl;
    std::cout << "Saisir une option : ";
    char choix;
    std::cin >> choix;
	cin.ignore(numeric_limits<streamsize>::max(),'\n');
	system("clear");	
	switch(choix){
			case 'R':
				retour = true;
				break;
	 	default:
			cout << "L'option saisie n'est pas valide!"<<endl;
			break;
		}
	}	
}


void ouvrirStream(PosteDePilotage* pp){

        bool retour = false;

        while(retour == false){
    std::cout << "Ouverture du stream sur web" << std::endl;
    cout << "Lien du stream : "<< pp->getRover()->getStream().c_str()<<endl;
    std::cout << "R. Retour" << std::endl;
    std::cout << "Saisir une option : ";
    char choix;
    std::cin >> choix;
	cin.ignore(numeric_limits<streamsize>::max(),'\n');
        system("clear");        
	switch(choix){
                        case 'R':
                                retour = true;
                                break;
                default:
                        cout << "L'option saisie n'est pas valide!"<<endl;
                        break;
                }
        }
}




void afficherEtatMine(PosteDeCommandement* pc,Mission* mission){


    std::cout << "===== Etat de la mine =====" << std::endl;
    cout << " 0 : Fausse alerte"<<endl;
    cout << " 1 : Mine detectée"<<endl;
    std::cout << "R. Retour" << std::endl;
    std::cout << "Saisir une option : ";
    char choix;
    std::cin >> choix;
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        system("clear");
        switch(choix){
		case '0':
			{
			pc->updateMineEtat(mission,0);
			cout << "Statut de la mine changé avec succès !"<<endl;
			break;
			}
		case '1':
			{
			pc->updateMineEtat(mission,1);
			cout << "Statut de la mine changé avec succès !"<<endl;
                        break;
			}
		case 'R':
                                return;
                                break;
                default:
                        cout << "L'option saisie n'est pas valide!"<<endl;
                        break;
                }
        
}




void afficherMenuNouvelleMission(PosteDeCommandement* pc){


bool retour = false;

        while(retour == false){
    std::cout << "=== Choisir un type de mission ===" << std::endl;
    std::cout << " 0 : Standby." << endl;
    std::cout << " 1 : Stop." << endl;
    std::cout << " 2 : Reprise de la mission." << endl;
    std::cout << " 3 : Demande de rapport." << endl;
    std::cout << " 4 : Deminage." << endl;
    std::cout << " 5 : Mesure." << endl;
    std::cout << " 6 : Retour." << endl<<endl;
    std::cout << "R. Retour" << std::endl;
    std::cout << "Saisir une option : ";
    char choix;
    std::cin >> choix;
	cin.ignore(numeric_limits<streamsize>::max(),'\n');

	system("clear");
    MissionType type;
                switch(choix){
			case '0':
				type = MissionType::Standby;
				envoyerMissionAPilotage(pc, type);
				break;
			case '1':
				type = MissionType::Stop;
				envoyerMissionAPilotage(pc, type);
				break;
			case '2':
				type = MissionType::RepriseDeMission;
				envoyerMissionAPilotage(pc, type);
				break;
			case '3':
				type = MissionType::DemandeDeRapport;
				envoyerMissionAPilotage(pc, type);
				break;
			case '4':
				type = MissionType::Deminage;
				envoyerMissionAPilotage(pc, type);
				break;
			case '5':
				type = MissionType::Mesure;
				envoyerMissionAPilotage(pc, type);
				break;
			case '6':
				type = MissionType::Retour;
				envoyerMissionAPilotage(pc,type);
                        case 'R':
                                retour = true;
                                break;
                default:
                        cout << "L'option saisie n'est pas valide!"<<endl;
                        break;
                }

        }

}



void envoyerMissionAPilotage(PosteDeCommandement* pc, MissionType type)
{


cout << "Liste des postes de pilotages."<< endl;
                auto pilotages = pc->getPilotages();
                vector<PosteDePilotage*> unusedPilotes;
		for(const auto& kv:pilotages){
			if(type == MissionType::Standby){
				
				if(estRobotOccupe(pc , kv.second->getRover()->getId()) && kv.second->getRover()->getEtat() != "En Attente"){
					unusedPilotes.push_back(kv.second);
				}			
			}
			else if(type == MissionType::Stop){

				if(estRobotOccupe(pc , kv.second->getRover()->getId())){
					unusedPilotes.push_back(kv.second);
				}			


			}
			else if (type == MissionType::RepriseDeMission){
				if(kv.second->getRover()->getEtat() == "En Attente"){

					unusedPilotes.push_back(kv.second);
				}	


			}else if(type == MissionType::DemandeDeRapport){
				
				unusedPilotes.push_back(kv.second);	
			}else{
				if(!estRobotOccupe(pc , kv.second->getRover()->getId())){
					unusedPilotes.push_back(kv.second);
				}
			}			
		}
                
		
		for(long unsigned int i = 0; i<unusedPilotes.size();i++){
                        cout << " "  << i << " : poste de pilotage : " << unusedPilotes[i]->getName().c_str() << endl;
                }
                cout << " R : Retour. " << endl;
                cout << endl;

                string choix;

                cout << "Choisir un ou plusieurs postes de pilotage à ajouter à la mission (separé par une ','): ";
                cin >> choix;
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
                cout << endl;
		system("clear");
		vector<string> listChoix;
		vector<PosteDePilotage*> pilotagesSelected;
			size_t pos = 0;
			string token;
			string delimiter =",";
			while ((pos = choix.find(delimiter)) != std::string::npos) {
			    token = choix.substr(0, pos);
			    listChoix.push_back(token);
			    choix.erase(0, pos + delimiter.length());
			}
			listChoix.push_back(choix);

			for(auto input : listChoix){

                        	if(is_number(input) && stoi(input) <= unusedPilotes.size()){
					pilotagesSelected.push_back(unusedPilotes[stoi(input)]);

                        	}

                        	else {if(input == "R"){
						
						return;
                        	        }else{

                        	                cout << "L'option saisie n'est pas valide!"<<endl;
                        	        }
                        	}

			}




	std::cout << "Titre de la mission : " ;
	
	string titre;
	
	std::getline(cin, titre);


	std::cout << "Contenu de la mission : " ;

        string content;

        std::getline(cin, content);


	Mission* m = new Mission(type,titre);
	m->setContent(content);
	if(type == MissionType::Deminage || type == MissionType::Mesure){
		


		cout << "Localisation:"<<endl;

		sqlite3* db;
        int rc = sqlite3_open("donnees.db", &db);
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de l'ouverture de la base de données : " << sqlite3_errmsg(db) << std::endl;
        sqlite3_close(db);
    }



        string query = "SELECT name FROM Lieu ";
    	sqlite3_stmt* stmt;
        rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);

        while(rc == SQLITE_BUSY){
        	sleep(100);
        	rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    	}
        if (rc != SQLITE_OK)
        {
              std::cerr << "Erreur lors de la préparation de la requête 2 : " << sqlite3_errmsg(db) << std::endl;
              return;
        }
           
	vector<string> listLieux;

        if(sqlite3_column_count(stmt)==0){
        	cout<<"Aucun lieu identifiee"<<endl;
        }
        else{
        	while(sqlite3_step(stmt) != SQLITE_DONE){
                         
			listLieux.push_back((char*)sqlite3_column_text(stmt,0));
        	}
        }

        sqlite3_finalize(stmt);

	sqlite3_close(db);	
	

	for(long unsigned int i = 0; i < listLieux.size();i++ ){
		cout<< " "<<i<< " : "<<listLieux[i] << endl;
		
		}
		cout << "Choisir un lieu ?" <<endl;
		string choix;
		cin >> choix;
	cin.ignore(numeric_limits<streamsize>::max(),'\n');
	
	m->addLieu(listLieux[stoi(choix)]);	
	}
	for(auto pp : pilotagesSelected){
		m->addPilotage(pp);

		if(type == MissionType::Standby){

			pp->getRover()->setEtat("En Attente");
		}else if(type == MissionType::RepriseDeMission){

			pp->getRover()->setEtat("Disponible");
		}
	}

	system("clear");

	pc->addMission(m);

	
	thread t(EnvoiMission,pc,m);
	t.detach();

    std::cout << "Mission envoyée au poste de pilotage !" << std::endl;
}




void afficherStatistiques()
{
                                             // Connexion à la base de données SQLite
	sqlite3* db= initialiserConnexion();
                                            // Requêtes SQL pour récupérer les statistiques demandées

	//on ne sait pas comment savoir si c'est une mine 
    std::string query1 = "SELECT COUNT(*) FROM Lieu WHERE presenceMine = 1 or presenceMine = -1";  // Nombre total de mines détectées
    sqlite3_stmt* stmt1;

    int rc1 = sqlite3_prepare_v2(db, query1.c_str(), -1, &stmt1, nullptr);
    if (rc1 != SQLITE_OK)
      {
        std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
        return;
      }

    rc1 = sqlite3_step(stmt1);
    if (rc1 == SQLITE_ROW)
      {
        int count = sqlite3_column_int(stmt1, 0);
        std::cout << "Nombre total de mines détectées : " << count << std::endl;
      }

    

    sqlite3_finalize(stmt1);


	cout << endl;
    query1 = "SELECT COUNT(*) FROM Lieu WHERE presenceMine = -1";  // Nombre total de mines détectées
    
    rc1 = sqlite3_prepare_v2(db, query1.c_str(), -1, &stmt1, nullptr);
    if (rc1 != SQLITE_OK)
      {
        std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
        return;
      }

    rc1 = sqlite3_step(stmt1);
    if (rc1 == SQLITE_ROW)
      {
        int count = sqlite3_column_int(stmt1, 0);
        std::cout << "Nombre total de mines déminees : " << count << std::endl;
      }



    sqlite3_finalize(stmt1);


    sqlite3_close(db);   // Fermeture de la connexion à la base de données
			 //
	cout << endl;
}

void afficherDatesHeures(Mission* mission)
{
                        // Connexion à la base de données SQLite
	sqlite3* db= initialiserConnexion();
                       // Requête SQL pour récupérer les dates et heures précises des données spécifiées

    std::string query = "SELECT dateDebut FROM Mission WHERE idMission = ?";
    sqlite3_stmt* stmt;

    int rc1 = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if (rc1 != SQLITE_OK)
      {
        std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
        return;
      }

                       // Liaison des paramètres à la requête préparée

    sqlite3_bind_int(stmt, 1, mission->getIdMission());

                      // Exécution de la requête et affichage des résultats

    while (sqlite3_step(stmt) == SQLITE_ROW)
      {
        std::string datetime = reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0));
        std::cout << "Date et heure : " << datetime << std::endl;
      }

    sqlite3_finalize(stmt);
	

    sqlite3_close(db);   // Fermeture de la connexion à la base de données
	
}


sqlite3* initialiserConnexion()
{
                                            // Code pour initialiser la connexion à la base de données SQLite
                                            // Utilisation du pointeur intelligent db pour gérer la ressource
	sqlite3* db;
    int rc = sqlite3_open("donnees.db", &db);
    if (rc != SQLITE_OK)
      {
        std::cerr << "Erreur lors de la connexion à la base de données : " << sqlite3_errmsg(db) << std::endl;
        return nullptr;
      }

    return db;
}


void afficherSelectionModifMission(PosteDeCommandement* pc){
	bool retour = false;
        while(retour == false){
    std::cout << "=== Choisir une mission ===" << std::endl;
    auto missions = pc->getMissions();
	vector<Mission*> missionsModif;
               for(auto mission : missions){
			if(mission->getEtat() != "Succes" && mission->getEtat() != "Echec" && mission->getEtat() != "Annulee"){
				missionsModif.push_back(mission);
			}
                }
                for(long unsigned int i = 0; i<missionsModif.size();i++){
                        cout << " "  << i << " : Titre : " << missionsModif[i]->getTitre().c_str()<< ", Etat : "<<missionsModif[i]->getEtat().c_str() << endl;
                }
   
    std::cout << "R. Retour" << std::endl;
    std::cout << "Saisir une option : ";
    string choix;
                cin >> choix;

    cin.ignore(numeric_limits<streamsize>::max(),'\n');

    system("clear");
    	if(is_number(choix) && stoi(choix)<= missionsModif.size()){

		afficherChoixModifMission(missionsModif[stoi(choix)], pc);
	}else
                if(choix == "R"){
		retour = true;
        }else {

		cout << "L'option saisie n'est pas valide!"<<endl;
	}
	}


}



void afficherSelectionTermineeMission(PosteDeCommandement* pc){
        bool retour = false;
        while(retour == false){
    std::cout << "=== Missions terminees ===" << std::endl;
    auto missions = pc->getMissions();
        vector<Mission*> missionsModif;
               for(auto mission : missions){
                        if(mission->getEtat() == "Succes" || mission->getEtat() == "Echec" || mission->getEtat() == "Annulee"){
                                missionsModif.push_back(mission);
                        }
                }
                for(long unsigned int i = 0; i<missionsModif.size();i++){
                        cout  << " Titre : " << missionsModif[i]->getTitre().c_str()<< ", Etat : "<<missionsModif[i]->getEtat().c_str() << endl;
                }

    std::cout << "R. Retour" << std::endl;
    std::cout << "Saisir une option : ";
    char choix;
                cin >> choix;

    cin.ignore(numeric_limits<streamsize>::max(),'\n');

    system("clear");
                if(choix == 'R'){
                retour = true;
        }else {

                cout << "L'option saisie n'est pas valide!"<<endl;
        }
	}


}




void afficherChoixModifMission(Mission* mission, PosteDeCommandement* pc){

	
bool retour = false;

        while(retour == false){

    std::cout << "=== Modifier un information de la mission  ===" << std::endl;
    std::cout << "Titre : " << mission->getTitre().c_str() << std::endl;
    std::cout << "Contenu : " << mission->getContent().c_str() << std::endl;
    std::cout << "Postes de pilotages : " << std::endl;
    for(auto pp : mission->getPilotages()){

        cout << pp->getName().c_str() << endl;
    }
    std::cout << "Type : " << mission->getMissionType().c_str() << std::endl<<endl;
    std::cout << "Etat : " << mission->getEtat().c_str() << endl;
    std::cout << "Lieu : ";
    for(auto lieu : mission->getLieux()){

	cout << lieu.c_str();
    }
    cout << endl;
    std::cout << " 0 : Modifier le titre. " << endl;
    std::cout << " 1 : Modifier le contenu. " << endl;
    std::cout << " 2 : Ajouter un pilote. " << endl;
    std::cout << " 3 : Modifier le type de mission. " << endl;
    cout << " 4 : Confirmer la presence d'une mine."<<endl;
    cout << " 5 : Envoyer a nouveau la mission au pilotes."<<endl;
    std::cout << "R. Retour" << std::endl;
    std::cout << "Saisir une option : ";
    char choix;
    std::cin >> choix;
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        system("clear");

	switch(choix){
                        case '0':
   				afficherModifTitreMission(mission);
   				break;
                        case '1':
				afficherModifContentMission(mission);
                                break;
			case '2':
				afficherAddPiloteMission(mission,pc);
				break;
			case '3':
				afficherModifTypeMission(mission);
				break;
			case '4':
				afficherEtatMine(pc,mission);
				break;
			case '5':
				{
					thread t(EnvoiMission,pc,mission);
        				t.detach();
					break;
				}
                        case 'R':
                                retour = true;
                                break;
			default:
                        cout << "L'option saisie n'est pas valide!"<<endl;
                        break;
                }

        }

}


void afficherModifTypeMission(Mission* mission){
std::cout << "=== Choisir un type de mission ===" << std::endl;
    std::cout << " 0 : Standby." << endl;
    std::cout << " 1 : Stop." << endl;
    std::cout << " 2 : Reprise de la mission." << endl;
    std::cout << " 3 : Demande de rapport." << endl;
    std::cout << " 4 : Deminage." << endl;
    std::cout << " 5 : Mesure." << endl;
    std::cout << " 6 : Retour." <<endl<<endl;
    std::cout << "R. Retour" << std::endl;
    std::cout << "Saisir une option : ";
    char choix;
    std::cin >> choix;
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
	system("clear");


	bool retour = false;
    MissionType type;
                switch(choix){
                        case '0':
                                type = MissionType::Standby;
                                break;
                        case '1':
                                type = MissionType::Stop;
                                break;
                        case '2':
                                type = MissionType::RepriseDeMission;
                                break;
                        case '3':
                                type = MissionType::DemandeDeRapport;
                                break;
                        case '4':
                                type = MissionType::Deminage;
                                break;
                        case '5':
                                type = MissionType::Mesure;
                                break;
			case '6':
				type = MissionType::Retour;
				break;
                        case 'R':
                                retour = true;
                                break;
                default:
				cout << "L'option saisie n'est pas valide!"<<endl;
				break;
		}

	if(retour == false){

		mission->setType(type);


		sqlite3* db= initialiserConnexion();
                       // Requête SQL pour récupérer les dates et heures précises des données spécifiées

    std::string query = "UPDATE Mission SET type = ? WHERE idMission = ?";
    sqlite3_stmt* stmt;

    int rc1 = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if (rc1 != SQLITE_OK)
      {
        std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
        return;
      }

                       // Liaison des paramètres à la requête préparée

    sqlite3_bind_text(stmt, 1, mission->getMissionType().c_str(), strlen(mission->getMissionType().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_int(stmt, 2, mission->getIdMission());

                      // Exécution de la requête et affichage des résultats

    if(sqlite3_step(stmt) != SQLITE_DONE)
      {
        std::cout << "Erreur d'exection de la requete " <<  std::endl;
      }

    sqlite3_finalize(stmt);


    sqlite3_close(db);	
	}
}
void afficherModifContentMission(Mission* mission){
	std::cout << "Saisir le nouveau contenu de la mission : " ;

        string content;

        std::getline(cin, content);
	system("clear");
        mission->setContent(content);


        sqlite3* db= initialiserConnexion();
                       // Requête SQL pour récupérer les dates et heures précises des données spécifiées

    std::string query = "UPDATE Mission SET content = ? WHERE idMission = ?";
    sqlite3_stmt* stmt;

    int rc1 = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if (rc1 != SQLITE_OK)
      {
        std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
        return;
      }

                       // Liaison des paramètres à la requête préparée

    sqlite3_bind_text(stmt, 1, mission->getContent().c_str(), strlen(mission->getContent().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_int(stmt, 2, mission->getIdMission());

                      // Exécution de la requête et affichage des résultats

    if(sqlite3_step(stmt) != SQLITE_DONE)
      {
        std::cout << "Erreur d'exection de la requete " <<  std::endl;
      }

    sqlite3_finalize(stmt);


    sqlite3_close(db);

}

void afficherAddPiloteMission(Mission* mission, PosteDeCommandement* pc){


	cout << "Liste des postes de pilotages."<< endl;
                auto pilotages = pc->getPilotages();
                vector<PosteDePilotage*> unusedPilotes;
                for(const auto& kv:pilotages){
      
                                if(!estRobotOccupe(pc , kv.second->getRover()->getId()) && std::find(mission->getPilotages().begin(),mission->getPilotages().end(),kv.second)!= mission->getPilotages().end()){
                                        unusedPilotes.push_back(kv.second);
                                }
                        
                }


                for(long unsigned int i = 0; i<unusedPilotes.size();i++){
                        cout << " "  << i << " : poste de pilotage : " << unusedPilotes[i]->getName().c_str() << endl;
                }
                cout << " R : Retour. " << endl;
                cout << endl;




		string choix;

                cout << "Choisir un ou plusieurs postes de pilotage à ajouter à la mission (separé par une ','): ";
                cin >> choix;
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
                cout << endl;
		system("clear");
                vector<string> listChoix;
                vector<PosteDePilotage*> pilotagesSelected;
                        size_t pos = 0;
                        string token;
                        string delimiter =",";
                        while ((pos = choix.find(delimiter)) != std::string::npos) {
                            token = choix.substr(0, pos);
                            listChoix.push_back(token);
                            choix.erase(0, pos + delimiter.length());
                        }
                        listChoix.push_back(choix);

                        for(auto input : listChoix){

                                if(is_number(input) && stoi(input) <= unusedPilotes.size()){
                                        pilotagesSelected.push_back(unusedPilotes[stoi(input)]);

                                }

                                else {if(input == "R"){

                                                return;
                                        }else{

                                                cout << "L'option saisie n'est pas valide!"<<endl;
                                        }
                                }

                        }




			for(auto pp : pilotagesSelected){
                mission->addPilotage(pp);

                if(mission->getMissionType() == "Standby"){

                        pp->getRover()->setEtat("En Attente");
                }else if(mission->getMissionType() == "RepriseDeMission"){

                        pp->getRover()->setEtat("Disponible");
                }





		sqlite3* db= initialiserConnexion();
                       // Requête SQL pour récupérer les dates et heures précises des données spécifiées

    std::string query = "INSERT INTO EstAttribueeA (idMission,idRover) VALUES (?,?)";
    sqlite3_stmt* stmt;

    int rc1 = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if (rc1 != SQLITE_OK)
      {
        std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
        return;
      }

                       // Liaison des paramètres à la requête préparée

    sqlite3_bind_int(stmt, 1, mission->getIdMission());
    sqlite3_bind_int(stmt, 2, pp->getRover()->getId());

                      // Exécution de la requête et affichage des résultats

    if(sqlite3_step(stmt) != SQLITE_DONE)
      {
        std::cout << "Erreur d'exection de la requete " <<  std::endl;
      }

    sqlite3_finalize(stmt);


    sqlite3_close(db);


        }







}
void afficherModifTitreMission(Mission* mission){


	std::cout << "Saisir le nouveau titre de la mission : " ;

        string titre;

        std::getline(cin, titre);

	mission->setTitre(titre);
	system("clear");

	sqlite3* db= initialiserConnexion();
                       // Requête SQL pour récupérer les dates et heures précises des données spécifiées

    std::string query = "UPDATE Mission SET titre = ? WHERE idMission = ?";
    sqlite3_stmt* stmt;

    int rc1 = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if (rc1 != SQLITE_OK)
      {
        std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
        return;
      }

                       // Liaison des paramètres à la requête préparée

    sqlite3_bind_text(stmt, 1, mission->getTitre().c_str(), strlen(mission->getTitre().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_int(stmt, 2, mission->getIdMission());

                      // Exécution de la requête et affichage des résultats

    if(sqlite3_step(stmt) != SQLITE_DONE)
      {
        std::cout << "Erreur d'exection de la requete " <<  std::endl;
      }

    sqlite3_finalize(stmt);


    sqlite3_close(db);

}




bool estRobotOccupe(PosteDeCommandement* posteDeCommandement, int idRobot) {
    auto missions = posteDeCommandement->getMissions();

    for (const auto& mission : missions) {
        auto pilotages = mission->getPilotages();

        for (const auto& pilotage : pilotages) {
            if (pilotage->getRover()->getId() == idRobot && (mission->getEtat() != "Succes" && mission->getEtat() != "Echec" && mission->getEtat() != "Annulee")) {
                return true; // Le robot est occupé par une mission en cours
            }
        }
    }

    return false; // Le robot n'est pas occupé par une mission en cours
}



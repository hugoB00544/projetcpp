#include "mission.h"

Mission::Mission(MissionType t, const std::string& vtitre, int vid)
    : type(t), titre(vtitre)
{
	if(vid != -1)
                idMission = vid;
        else
                idMission = Mission::nextIdMission;


        if(vid>Mission::nextIdMission)
            Mission::nextIdMission=vid+1;
        else
                Mission::nextIdMission++;

    etat = "En attente d'envoi";
}

void Mission::infos() const
{
    std::cout << "Mission numero " << idMission << std::endl;
    std::cout << "Type de Mission: " << getMissionType() << std::endl;
}

int Mission::getIdMission() const
{
    return idMission;
}

void Mission::addLieu(const std::string& lieu)
{
	if ( std::find(lieux.begin(), lieux.end(), lieu) == lieux.end() ){
    		lieux.push_back(lieu);
		updateBddLieu(lieu);
	}
}


void Mission::addPilotage(PosteDePilotage* pp)
{
	if ( std::find(pilotages.begin(), pilotages.end(), pp) == pilotages.end() ){
		pilotages.push_back(pp);
		updateBddPilote(pp);
	}

}

vector<PosteDePilotage*> Mission::getPilotages() const
{
    return pilotages;
}

std::string Mission::getMissionType() const
{
    switch (type)
    {
    case MissionType::Standby:
        return "Standby";
    case MissionType::Stop:
        return "Stop";
    case MissionType::RepriseDeMission:
        return "Reprise de la mission";
    case MissionType::DemandeDeRapport:
        return "Demande de rapport";
    case MissionType::Deminage:
        return "Deminage";
    case MissionType::Mesure:
        return "Mesures";
	break;
    case MissionType::Retour:
	return "Retour";
    default:
        return "Type non identifie";
    }
}

std::string Mission::getTitre() const
{
    return titre;
}

json Mission::getJson(PosteDePilotage* pp) const
{
	string lieu;
	if(lieux.empty())
		lieu = "Aucun lieu";
	else
		lieu = lieux[0];
    json jsonMission =
    {
        {"Type", getMissionType()},
        {
            "Corps", {
        	{"idMission", getIdMission()},
		{"idPC", 6},
		{"idPersonnage",pp->getId()},
		{"idRover",pp->getRover()->getId()},
                {"missionTitle", getTitre()},
		{"missionCurrentState", getEtat()},
		{"missionContent",getContent()},
		{"missionStartDate",getDateDebut()},
		{"missionLocation",lieu},
		{"missionType", pp->getType()}
            }
        }
    };
    return jsonMission;
}

void Mission::SendMission()
{
	auto pilotages = getPilotages();
	for(auto pp : pilotages ){
		thread t(&Mission::SendMissionPilote, this, pp);
		t.detach();
	}

}

void Mission::SendMissionPilote(PosteDePilotage* pp)
{	
        std::time_t start_time = time(0);
	std::tm* now = std::localtime(&start_time);
	char timeString[20];
	strftime(timeString,20,"%d/%m/%Y %T",now);
	dateDebut = timeString;

    // Cr�ation du socket
    int clientSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (clientSocket < 0)
    {
        std::cerr << "Erreur lors de la cr�ation du socket." << std::endl;
        return ;
    }

    // Configuration de l'adresse du serveur distant
    struct sockaddr_in serverAddress {};
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(pp->getPort()); // Num�ro de port du serveur distant
    serverAddress.sin_addr.s_addr = inet_addr(pp->getIp().c_str()); // Adresse IP du serveur distant


    struct timeval timeout;
    timeout.tv_sec = 5;
    timeout.tv_usec = 0;

    if(setsockopt(clientSocket, SOL_SOCKET, SO_SNDTIMEO, (const char*)&timeout, sizeof(timeout)) < 0 || setsockopt(clientSocket,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout)) < 0)
    {
        cerr << "Erreur durant la config du delai de connexion " << endl;
        close(clientSocket);
        return ;
    }





    // Connexion au serveur distant

    auto start = chrono::steady_clock::now();

    if (connect(clientSocket, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) < 0)
    {
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<chrono::seconds>(end-start).count();


        std::cerr << "Erreur lors de la connexion au serveur " << pp->getIp().c_str() << std::endl;
        cerr << "Temps ecoule : " << elapsed << " secondes."<< endl;
        return ;
    }

    auto end = chrono::steady_clock::now();
    auto elapsed = chrono::duration_cast<chrono::seconds>(end-start).count();




    //send to sever and receive from server

	setEtat("En cours");
    string sjson = getJson(pp).dump();
    //string sjson = "{\"IdMission\":1}";
    send(clientSocket,sjson.c_str(),sjson.length(),0);

    close(clientSocket);

    updateBddMission();
    return ;
}


void Mission::updateBddMission(){

	sqlite3* db;
	int rc = sqlite3_open("donnees.db", &db);
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de l'ouverture de la base de donn�es : " << sqlite3_errmsg(db) << std::endl;
        sqlite3_close(db);
    }

	std::string query = "UPDATE Mission SET titre = ?, etat = ?, type = ?, dateDebut = ?, dateFin = ? WHERE idMission = ?;";
    sqlite3_stmt* stmt;

    rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    while(rc == SQLITE_BUSY){

	sleep(100);
	rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    }
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de la pr�paration de la requ�te : " << sqlite3_errmsg(db) << std::endl;
        return;
    }
    sqlite3_bind_text(stmt, 1, getTitre().c_str(), strlen(getTitre().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 2, getEtat().c_str(), strlen(getEtat().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 3, getMissionType().c_str(), strlen(getMissionType().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 4, getDateDebut().c_str(), strlen(getDateDebut().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 5, getDateFin().c_str(), strlen(getDateFin().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_int(stmt, 6, getIdMission());

    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE)
    {
        std::cerr << "Erreur lors de l'ex�cution de la requ�te : " << sqlite3_errmsg(db) << std::endl;
    }

    sqlite3_finalize(stmt);

}


void Mission::updateBddLieu(string lieu){

        sqlite3* db;
        int rc = sqlite3_open("donnees.db", &db);
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de l'ouverture de la base de donn�es : " << sqlite3_errmsg(db) << std::endl;
        sqlite3_close(db);
    }



	string query = "SELECT idLieu FROM Lieu WHERE name like ?";

    sqlite3_stmt* stmt;
                rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
                
		
		while(rc == SQLITE_BUSY){

        sleep(100);
        rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    }

		if (rc != SQLITE_OK)
                {
                    std::cerr << "Erreur lors de la pr�paration de la requ�te 2 : " << sqlite3_errmsg(db) << std::endl;
                    return;
                }

                sqlite3_bind_text(stmt, 1, lieu.c_str(), strlen(lieu.c_str()), SQLITE_STATIC);
		

		int idLieu = -1;

                if(sqlite3_column_count(stmt)==0){
                        cerr<<"Lieu inexistant"<<endl;
                    }
                else{
                        while(sqlite3_step(stmt) != SQLITE_DONE){
                                idLieu = sqlite3_column_int(stmt,1);
                        }
                 }

                sqlite3_finalize(stmt);
	
		if(idLieu != -1){

	
	
			string query = "SELECT * FROM EstLocaliseeA WHERE idMission =  ? and idLieu = ?";

    sqlite3_stmt* stmt;
                rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);


                while(rc == SQLITE_BUSY){

        sleep(100);
        rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    }

                if (rc != SQLITE_OK)
                {
                    std::cerr << "Erreur lors de la pr�paration de la requ�te 2 : " << sqlite3_errmsg(db) << std::endl;
                    return;
                }

                sqlite3_bind_int(stmt, 1, getIdMission());
                sqlite3_bind_int(stmt, 2, idLieu);


                bool existe = false;

                if(sqlite3_column_count(stmt)>0){
                        existe = true;
                }

                sqlite3_finalize(stmt);


        if(!existe){

	
	
	
	
	
	
	
			query = "INSERT INTO EstLocaliseeA (idMission,idLieu) VALUES (?, ?);";

                rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);


		while(rc == SQLITE_BUSY){

        sleep(100);
        rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    }

		if (rc != SQLITE_OK)
                {
                        std::cerr << "Erreur lors de la pr�paration de la requ�te : " << sqlite3_errmsg(db) << std::endl;
                        return;
                }

                 sqlite3_bind_int(stmt, 1, getIdMission());
                 sqlite3_bind_int(stmt, 2, idLieu);

                 rc = sqlite3_step(stmt);
                 if (rc != SQLITE_DONE)
                {
                        std::cerr << "Erreur lors de l'ex�cution de la requ�te : " << sqlite3_errmsg(db) << std::endl;
                    }

                    sqlite3_finalize(stmt);
		}

		}
}

void Mission::updateBddPilote(PosteDePilotage* pp){

        sqlite3* db;
        int rc = sqlite3_open("donnees.db", &db);
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de l'ouverture de la base de donn�es : " << sqlite3_errmsg(db) << std::endl;
        sqlite3_close(db);
    }




string query = "SELECT * FROM EstAttribueeA WHERE idMission =  ? and idRover = ?";

    sqlite3_stmt* stmt;
                rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);


                while(rc == SQLITE_BUSY){

        sleep(100);
        rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    }

                if (rc != SQLITE_OK)
                {
                    std::cerr << "Erreur lors de la pr�paration de la requ�te 2 : " << sqlite3_errmsg(db) << std::endl;
                    return;
                }

                sqlite3_bind_int(stmt, 1, getIdMission());
                sqlite3_bind_int(stmt, 2, pp->getRover()->getId());


                bool existe = false;

                if(sqlite3_column_count(stmt)>0){
                	existe = true;    
		}

                sqlite3_finalize(stmt);


	if(!existe){


	query = "INSERT INTO EstAttribueeA (idMission,idRover) VALUES (?, ?);";
        
                rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);

		while(rc == SQLITE_BUSY){

        sleep(100);
        rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    }

		if (rc != SQLITE_OK)
                {
                        std::cerr << "Erreur lors de la pr�paration de la requ�te 1 : " << sqlite3_errmsg(db) << std::endl;
                        return;
                }

                sqlite3_bind_int(stmt, 1, getIdMission());
                sqlite3_bind_int(stmt, 2, pp->getRover()->getId());

                rc = sqlite3_step(stmt);
                if (rc != SQLITE_DONE)
                {
                        std::cerr << "Erreur lors de l'ex�cution de la requ�te 1 : " << sqlite3_errmsg(db) << std::endl;
                }

                sqlite3_finalize(stmt);
	}
}



bool Mission::estAttribueeA(PosteDePilotage* pp){

	return (find(pilotages.begin(),pilotages.end(),pp) != pilotages.end());


}

int Mission::nextIdMission = 0;


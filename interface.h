#include "posteDeCommandement.h"
#include "mission.h"


bool is_number(const std::string& );

void Demarrage(PosteDeCommandement*);

void EnvoiMission(PosteDeCommandement*,Mission* );
	
void interfaceStart();


void accueil(PosteDeCommandement*);


void selectionPilotage (PosteDeCommandement*);



void afficherAvancementOperation();

void afficherLogs();

void ajouterPosteDePilotage(PosteDeCommandement*);

void ajouterRover(PosteDePilotage*);

void afficherInfoPilotage(PosteDeCommandement*,PosteDePilotage*);


void afficherMenuNouvelleMission(PosteDeCommandement*);


void envoyerMissionAPilotage(PosteDeCommandement* , MissionType);


void afficherListeMission(PosteDeCommandement*, PosteDePilotage*);


void ouvrirStream(PosteDePilotage*);


void afficherStatistiques();  // Affiche les statistiques demandées


void afficherDatesHeures(Mission*);  // Affiche les dates et heures précises des données spécifiées
				     //
sqlite3* initialiserConnexion();


bool estRobotOccupe(PosteDeCommandement*, int idRobot); // Vérifie si un robot spécifié par son ID est occupé
							//
void afficherSelectionModifMission(PosteDeCommandement*);

void afficherChoixModifMission(Mission*, PosteDeCommandement*);

void afficherModifTypeMission(Mission* mission);

void afficherModifContentMission(Mission* mission);

void afficherModifTitreMission(Mission* mission);

void afficherAddPiloteMission(Mission* mission,PosteDeCommandement*);

void afficherSelectionTermineeMission(PosteDeCommandement*);

void afficherChoixTypeListeMissions(PosteDeCommandement*);

void afficherEtatMine(PosteDeCommandement*, Mission*);

#ifndef ROVER_H
#define ROVER_H

#include <iostream>

class Rover
{
private:
    int id;
    std::string ip;
    int port;
    std::string name;
    std::string type;
    static int idRover;
    std::string etat;
    std::string stream;

public:
    Rover(const std::string& vip, int vport, const std::string& vname, const std::string& vtype, int vid = -1);
    ~Rover();

    int getId() const;
    std::string getIp() const;
    int getPort() const;
    std::string getName() const;
    std::string getType() const;
    std::string getEtat() const {return etat;};
    std::string getStream() const {return stream;};
    void setEtat(std::string vetat){etat = vetat;};
    void setStream(std::string vstream){stream = vstream;};
};

#endif // ROVER_H


#include "posteDeCommandement.h"

PosteDeCommandement::PosteDeCommandement()
{
    int rc = sqlite3_open("donnees.db", &db);
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de l'ouverture de la base de données : " << sqlite3_errmsg(db) << std::endl;
        sqlite3_close(db);
    }
}

PosteDeCommandement::~PosteDeCommandement()
{
    sqlite3_close(db);
}

void PosteDeCommandement::envoyerOrdre(Mission& mission)
{
    enregistrerOrdreMission(mission);
	mission.SendMission();

    // Enregistrer l'ordre de mission dans la base de données

    auto pilotages = mission.getPilotages(); // Récupération de l'adresse IP du robot
    // Vérifier si le robot a effectué 10 missions consécutives
    for(auto pp:pilotages){
    	if (robotAEffectue10Missions(pp->getRover()->getId()))
    	{
    	    effectuerRevision(pp->getRover()->getId());
    	}
    }
}

void PosteDeCommandement::enregistrerOrdreMission( const Mission& mission)
{


	bool newMission;
	std::string query = "SELECT COUNT(*) FROM Mission WHERE idMission = ?";
	sqlite3_stmt* stmt;
                int rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
                if (rc != SQLITE_OK)
                {
                    std::cerr << "Erreur lors de la préparation de la requête 2 : " << sqlite3_errmsg(db) << std::endl;
                    return;
                }
                sqlite3_bind_int(stmt, 1, mission.getIdMission());
                rc = sqlite3_step(stmt);



		if (rc != SQLITE_ROW)
    		{
    		    std::cerr << "Erreur lors de l'exécution de la requête : " << sqlite3_errmsg(db) << std::endl;
        
    		}

    		int nbMission = sqlite3_column_int(stmt, 0);


		cout << sqlite3_column_count(stmt)<<endl;
                if(nbMission==0){
                        newMission = true;
                    }
                else{
                                newMission = false;
                 }
                sqlite3_finalize(stmt);

		
	if(newMission){

    // Construction de la requête SQL pour insérer l'ordre de mission dans la base de données
    query = "INSERT INTO Mission (idMission, titre, etat, type, dateDebut, content) VALUES (?, ?, ?, ?, ?,?);";
    

    rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de la préparation de la requête 1 : " << sqlite3_errmsg(db) << std::endl;
        return;
    }
    sqlite3_bind_int(stmt, 1, mission.getIdMission());
    sqlite3_bind_text(stmt, 2, mission.getTitre().c_str(), strlen(mission.getTitre().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 3, mission.getEtat().c_str(), strlen(mission.getEtat().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 4, mission.getMissionType().c_str(), strlen(mission.getMissionType().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 5, mission.getDateDebut().c_str(), strlen(mission.getDateDebut().c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 6, mission.getContent().c_str(), strlen(mission.getContent().c_str()), SQLITE_TRANSIENT);

    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE)
    {
        std::cerr << "Erreur lors de l'exécution de la requête 1 : " << sqlite3_errmsg(db) << std::endl;
    }

    sqlite3_finalize(stmt);
	

	auto pilotages = mission.getPilotages();


	for(auto pp:pilotages){

		// Construction de la requête SQL pour insérer l'ordre de mission dans la base de données
    		query = "INSERT INTO EstAttribueeA (idMission,idRover) VALUES (?, ?);";
    		

    		rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    		if (rc != SQLITE_OK)
    		{
        		std::cerr << "Erreur lors de la préparation de la requête 1 : " << sqlite3_errmsg(db) << std::endl;
        		return;
    		}

    		sqlite3_bind_int(stmt, 1, mission.getIdMission());
    		sqlite3_bind_int(stmt, 2, pp->getRover()->getId());

    		rc = sqlite3_step(stmt);
    		if (rc != SQLITE_DONE)
    		{
        		std::cerr << "Erreur lors de l'exécution de la requête 1 : " << sqlite3_errmsg(db) << std::endl;
    		}

    		sqlite3_finalize(stmt);
	}


	if(mission.getLieux().size() >0){
	int idLieu = -1;
    
	auto lieux = mission.getLieux();
	
	for(auto lieu:lieux){
		query = "SELECT idLieu FROM Lieu WHERE name = ?";

    		rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    		if (rc != SQLITE_OK)
    		{
    		    std::cerr << "Erreur lors de la préparation de la requête 2 : " << sqlite3_errmsg(db) << std::endl;
    		    return;
    		}
    		sqlite3_bind_text(stmt, 1, lieu.c_str(), strlen(lieu.c_str()), SQLITE_TRANSIENT);
		rc = sqlite3_step(stmt);
		
		//cout << query << sqlite3_column_count(stmt) << sqlite3_column_int(stmt,0) << endl;
    		if(sqlite3_column_count(stmt)==0){
			cerr<<"Lieu inexistant"<<endl;
		    }
    		else{
				idLieu = sqlite3_column_int(stmt,0);
   		 }
    		sqlite3_finalize(stmt);
		if(idLieu > 0){
    		query = "INSERT INTO EstLocaliseeA (idMission,idLieu) VALUES (?, ?);";

    		rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    		if (rc != SQLITE_OK)
    		{
        		std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
        		return;
    		}

   		 sqlite3_bind_int(stmt, 1, mission.getIdMission());
   		 sqlite3_bind_int(stmt, 2, idLieu);

   		 rc = sqlite3_step(stmt);
   		 if (rc != SQLITE_DONE)
    		{
        		std::cerr << "Erreur lors de l'exécution de la requête : " << sqlite3_errmsg(db) << std::endl;
		    }

		    sqlite3_finalize(stmt);
				}
			}
		}
	}
}

bool PosteDeCommandement::robotAEffectue10Missions(int roverId)
{
    // Récupérer le nombre de missions effectuées par le robot depuis la base de données
    std::string query = "SELECT COUNT(*) FROM EstAttribueeA WHERE idRover = ?";
    sqlite3_stmt* stmt;

    int rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
        return false;
    }

    sqlite3_bind_int(stmt, 1, roverId);

    rc = sqlite3_step(stmt);
    if (rc != SQLITE_ROW)
    {
        std::cerr << "Erreur lors de l'exécution de la requête : " << sqlite3_errmsg(db) << std::endl;
        return false;
    }

    int nbMissions = sqlite3_column_int(stmt, 0);

    sqlite3_finalize(stmt);

    // Vérifier si le nombre de missions atteint 10
    return nbMissions >= 10;
}

void PosteDeCommandement::effectuerRevision(int robotId)
{
    // Ordre d'effectuer la révision envoyé au robot

    // Enregistrer la révision dans la base de données
    std::string query = "UPDATE Rover SET etat = 'En Revision' where idRover = ?";
    sqlite3_stmt* stmt;

    int rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
        return;
    }

    sqlite3_bind_int(stmt, 1, robotId);

    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE)
    {
        std::cerr << "Erreur lors de l'exécution de la requête : " << sqlite3_errmsg(db) << std::endl;
    }

    sqlite3_finalize(stmt);
}




void PosteDeCommandement::DemarrageServer()
{
   
	int serverSocketId = socket(AF_INET, SOCK_STREAM, 0);

	if (serverSocketId < 0) {
	    std::cerr << "Erreur lors de la création du socket." << std::endl;
	    return;
	}

	struct sockaddr_in serverAddress{};
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(8080);
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(serverSocketId, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) < 0) {
	    std::cerr << "Erreur lors de la liaison du socket du poste de commandement." << std::endl;
	    return;
	}
	if (listen(serverSocketId, 5) < 0) {
	    std::cerr << "Erreur lors du listen." << std::endl;
	    return;
	}

	while (true) {
	    struct sockaddr_in clientAddress{};
	    socklen_t clientAddressLength = sizeof(clientAddress);
	    int clientSocketId = accept(serverSocketId, (struct sockaddr*)&clientAddress, &clientAddressLength);

	    if (clientSocketId < 0) {
	        std::cerr << "Erreur lors de l'acceptation de la connexion." << std::endl;
	        continue;
	    }

	    getpeername(clientSocketId, (struct sockaddr *)&clientAddress, &clientAddressLength);
	    std::string client_ip = inet_ntoa(clientAddress.sin_addr);
	
	    char buffer[90000] = {0};
	    bzero(buffer, 90000);
	    ssize_t bytesRead = read(clientSocketId, buffer, sizeof(buffer));

	    if (bytesRead > 0) {

	        std::string message(buffer, bytesRead);
		std::string reponse = "Message recues avec succes!";
		send(clientSocketId,reponse.c_str(),reponse.length(),0);
		// Traitement du message reçu
		if(json::accept(message)){
	    		std::cout << "Client IP: " << client_ip << std::endl;

			thread t(&PosteDeCommandement::AnalyseDuJson, this, message);
	                t.detach();
		}
	    }
	    close(clientSocketId);
	}
}


void PosteDeCommandement::AnalyseDuJson(string message){
		json json_message = json::parse(message);
        	std::cout << "Message reçu : " << std::endl;


		cout << "Recu par " << json_message["sender"].get<string>()<<endl;
		cout << "type : "<<json_message["type"].get<string>()<<endl;
		cout << "date : " <<json_message["dateHour"].get<string>()<<endl;
		if(json_message.find("messageText") != json_message.end()){
			cout << "message : "<<json_message["messageText"].get<string>()<<endl;
		}
		Mission* mission;
		if(json_message.find("idMission") != json_message.end()){
			for(auto m : missions){
				if(m->getIdMission() == json_message["idMission"].get<int>()){
					mission = m;
					break;
				}

			}
		
		cout << "mission : "<<mission->getTitre().c_str()<<endl;

		}

		string type = json_message["type"].get<string>();

			string date = json_message["dateHour"].get<string>();
			string sender = json_message["sender"].get<string>();
			std::replace(date.begin(),date.end(),' ','-');
		if(type == "image"){

			string imgName = json_message["file"]["name"].get<string>();
			string message = date + " : " + sender + " , image : "+imgName; 
			ajoutLogs(message);


			string fileData = json_message["file"]["content"].get<string>();
                        string fileName = json_message["file"]["name"].get<string>();
		

			string filePath = "images/"+date+"_"+fileName;

			std::vector<unsigned char> imageData = base64ToBytes(fileData);

			
        		std::ofstream outputFile(filePath, std::ios::binary);
    			if (outputFile) {
				cout <<"Chemin de l'image : " << filePath << endl;
   			     outputFile.write(reinterpret_cast<const char*>(imageData.data()), imageData.size());
   			 } else {
        			std::cerr << "Erreur lors de la sauvegarde du fichier décodée." << std::endl;
    			}

			ajoutImageBdd(json_message["idRover"].get<int>(),date, filePath);

		}else if(type == "mesure"){
			if(json_message.find("file") != json_message.end()){

                        string message = date + " : "+sender+" mesure : "+json_message["file"]["content"]["unite"].get<string>()+" => "+std::to_string(json_message["file"]["content"]["valeur"].get<int>());
			cout << message << endl;
			ajoutLogs(message);
                }
			
				
			
			
			
			
			
		}else if(type == "message"){
			
			string messageText = json_message["messageText"].get<string>();
                        string message = date + " : " + sender + ", message : "+messageText;
                        ajoutLogs(message);	
			
			
		}else if(type == "rapport" ){

			string messageText = json_message["messageText"].get<string>();
			string message = date + " : " + sender + ", message : "+messageText;
			ajoutLogs(message);

			string fileData = json_message["file"]["content"].get<string>();
                        string fileName = json_message["file"]["name"].get<string>();
			

			

			string filePath = "rapports/"+date+"_"+fileName;

                        std::vector<unsigned char> data = base64ToBytes(fileData);


                        std::ofstream outputFile(filePath, std::ios::binary);
                        if (outputFile) {
				cout <<"Rapport : " << data.data() << endl;
                             outputFile.write(reinterpret_cast<const char*>(data.data()), data.size());
                         } else {
                                std::cerr << "Erreur lors de la sauvegarde du fichier décodée." << std::endl;
                        }
			mission->setDateFin(date);

			int idPilote = -1;
			if(json_message["idPersonne"].type() == json::value_t::number_integer){
				idPilote = json_message["idPersonne"].get<int>();

			}else if(json_message["idPersonne"].type() == json::value_t::string){

				idPilote = atoi(json_message["idPersonne"].get<string>().c_str());
			}
			ajoutRapportBdd(mission,idPilote,date,filePath);

			if(messageText == "Succes de la mission"){
				mission->setEtat("Succes");
				if(mission->getMissionType() == "Deminage"){

					updateMineEtat(mission,-1);

				}
			}else if(messageText == "Echec de la mission"){

				mission->setEtat("Echec");
			}


		}
            
    

}


void PosteDeCommandement::ajoutRapportBdd(Mission* mission,int ppId,string date, string path){

	std::string query = "INSERT INTO RapportMission (date,contenu,idPilotage,idMission) VALUES(?,?,?,?)";
    sqlite3_stmt* stmt;

    int rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
        return;
    }


        sqlite3_bind_text(stmt, 1, date.c_str(), strlen(date.c_str()), SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 2, path.c_str(), strlen(path.c_str()), SQLITE_TRANSIENT);
        sqlite3_bind_int(stmt, 3, ppId);
    sqlite3_bind_int(stmt, 4, mission->getIdMission());

    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE)
    {
        std::cerr << "Erreur lors de l'exécution de la requête : " << sqlite3_errmsg(db) << std::endl;
    }

        sqlite3_finalize(stmt);
}
void PosteDeCommandement::ajoutImageBdd(int idRover,string date, string path){


	std::string query = "INSERT INTO image (path,date,idRover) VALUES(?,?,?)";
    sqlite3_stmt* stmt;

    int rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
        return;
    }


    sqlite3_bind_text(stmt, 1, path.c_str(), strlen(path.c_str()), SQLITE_TRANSIENT);
	sqlite3_bind_text(stmt, 2, date.c_str(), strlen(date.c_str()), SQLITE_TRANSIENT);
	sqlite3_bind_int(stmt, 3, idRover);

    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE)
    {
        std::cerr << "Erreur lors de l'exécution de la requête : " << sqlite3_errmsg(db) << std::endl;
    }

	sqlite3_finalize(stmt);


}


void PosteDeCommandement::updateMineEtat(Mission* mission, int etat){

	string lieu = mission->getLieux()[0];
	int idLieu = -1;
	std::string query = "SELECT idLieu FROM Lieu WHERE name = ?";

	sqlite3_stmt* stmt;
        int rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
        if (rc != SQLITE_OK)
        {
             std::cerr << "Erreur lors de la préparation de la requête 2 : " << sqlite3_errmsg(db) << std::endl;
             return;
         }
         sqlite3_bind_text(stmt, 1, lieu.c_str(), strlen(lieu.c_str()), SQLITE_TRANSIENT);
         rc = sqlite3_step(stmt);
	 if(sqlite3_column_count(stmt)>0){
               	idLieu = sqlite3_column_int(stmt,0);
         }
         sqlite3_finalize(stmt);



	if(idLieu != -1){


        	query = "UPDATE Lieu SET presenceMine = ? WHERE idLieu = ?";
   

    		rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    		if (rc != SQLITE_OK)
    		{
    		    std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
    		    return;
    
		}

		sqlite3_bind_int(stmt, 1, etat);
        	sqlite3_bind_int(stmt, 2, idLieu);

    		rc = sqlite3_step(stmt);
    		if (rc != SQLITE_DONE)
    		{
    		    std::cerr << "Erreur lors de l'exécution de la requête : " << sqlite3_errmsg(db) << std::endl;
    		}

        	sqlite3_finalize(stmt);

		
	}
		
}


void PosteDeCommandement::ajoutLogs(string message){
	ofstream myfile;
  	myfile.open("logs/logs.txt", std::ios_base::app);
  	myfile << message.c_str() << endl;
  	myfile.close();



}



std::vector<unsigned char> base64ToBytes(const std::string& base64String) {
    std::string decodedString = base64_decode(base64String);
    return std::vector<unsigned char>(decodedString.begin(), decodedString.end());
}


void transcrireJSONEnFichierImage(const string& data, const std::string& chemin)
{

    std::vector<unsigned char> imageData = base64ToBytes(data);


	std::ofstream outputFile(chemin, std::ios::binary);
    if (outputFile) {
        outputFile.write(reinterpret_cast<const char*>(imageData.data()), imageData.size());
    } else {
        std::cerr << "Erreur lors de la sauvegarde du fichier décodée." << std::endl;
    }
	



}





void PosteDeCommandement::ajoutPilotage(PosteDePilotage* pp){
	if(pilotages.count(pp->getId())==0){
		pilotages[pp->getId()] = pp;





		

	}else
		cerr << "Le poste de pilotage existe deja! "<< endl;





}



void PosteDeCommandement::initDB()
{

        
    // Construction de la requête SQL pour insérer l'ordre de mission dans la base de données
    std::string query = "SELECT p.idPilotage, p.ip, p.port, p.name, p.type, r.idRover,r.ip,r.port,r.name,r.type, r.etat, r.stream FROM PosteDePilotage AS p inner join Rover AS r on r.idPilotage = p.idPilotage";
    sqlite3_stmt* stmt;

    int rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
        return;
    }

	while(sqlite3_step(stmt) != SQLITE_DONE){
		int idpp =  sqlite3_column_int(stmt,0);
		string ippp = (char*)sqlite3_column_text(stmt,1);
		int portpp = sqlite3_column_int(stmt,2);
		string namepp = (char*)sqlite3_column_text(stmt,3);
		string typepp = (char*)sqlite3_column_text(stmt,4);
		

		PosteDePilotage* pp = new PosteDePilotage(ippp,portpp,namepp,typepp,idpp);


		int idr =  sqlite3_column_int(stmt,5);
		string ipr = (char*)sqlite3_column_text(stmt,6);
		int portr = sqlite3_column_int(stmt,7);
		string namer = (char*)sqlite3_column_text(stmt,8);
		string typer = (char*)sqlite3_column_text(stmt,9);
		string etatr = (char*)sqlite3_column_text(stmt,10);
		
	
		Rover* rover = new Rover(ipr,portr,namer,typer,idr);
		
		rover->setEtat(etatr);
			
		if(sqlite3_column_type(stmt,11) != SQLITE_NULL)		
		{	string streamr = (char*)sqlite3_column_text(stmt,11);
		
			rover->setStream(streamr);
		}
		
		pp->setRover(rover);
		ajoutPilotage(pp);

	}

    sqlite3_finalize(stmt);





    // Construction de la requête SQL pour insérer l'ordre de mission dans la base de données
    query = "SELECT mission.idMission, mission.titre, mission.etat, mission.type, mission.dateDebut, mission.dateFin,mission.content, rover.idPilotage from mission left join EstLocaliseeA on mission.idMission = EstLocaliseeA.idMission left join Lieu on Lieu.idLieu = EstLocaliseeA.idLieu inner join EstAttribueeA on EstAttribueeA.idMission = mission.idMission inner join rover on rover.idRover = EstAttribueeA.idRover";

    rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
        return;
    }
	Mission* mission;
        while(sqlite3_step(stmt) != SQLITE_DONE){

		if(!haveMissionById(sqlite3_column_int(stmt,0))){
			string type = (char*)sqlite3_column_text(stmt,3);
		mission = new Mission(getMissionTypeFromString(type),(char*)sqlite3_column_text(stmt,1),sqlite3_column_int(stmt,0));
		
		if(sqlite3_column_type(stmt,2) != SQLITE_NULL)		
			mission->setEtat((char*)sqlite3_column_text(stmt,2));
		if(sqlite3_column_type(stmt,4) != SQLITE_NULL)		
				mission->setDateDebut((char*)sqlite3_column_text(stmt,4));
		if(sqlite3_column_type(stmt,5) != SQLITE_NULL)		
				mission->setDateFin((char*)sqlite3_column_text(stmt,5));
		if(sqlite3_column_type(stmt,6) != SQLITE_NULL)		
				mission->setContent((char*)sqlite3_column_text(stmt,6));

		addMission(mission);
		}else{

			mission = getMissionById(sqlite3_column_int(stmt,0));

		}
		PosteDePilotage* pp = pilotages[sqlite3_column_int(stmt,7)];
		mission->addPilotage(pp);


        }

    sqlite3_finalize(stmt);


    // Construction de la requête SQL pour insérer l'ordre de mission dans la base de données
    query = "SELECT max(idMission) from mission";

    rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
    {
        std::cerr << "Erreur lors de la préparation de la requête : " << sqlite3_errmsg(db) << std::endl;
        return;
    }

        while(sqlite3_step(stmt) != SQLITE_DONE){
                Mission::nextIdMission =  sqlite3_column_int(stmt,0)+1;
        }

    sqlite3_finalize(stmt);

}

void PosteDeCommandement::addMission(Mission* mission){

        if(find(missions.begin(),missions.end(), mission) == missions.end()){
                missions.push_back(mission);

        }

}


bool PosteDeCommandement::haveMissionById(int id){

	for(auto mission : missions){
		if(mission->getIdMission()==id)
			return true;
	}
	return false;
}




Mission* PosteDeCommandement::getMissionById(int id){

	for(auto mission : missions){
                if(mission->getIdMission()==id)
                        return mission;
        }
	return nullptr;
}


MissionType getMissionTypeFromString(string type){
	if(type == "Standby")
		return MissionType::Standby;
	if(type == "Stop")
		return MissionType::Stop;
	if(type == "Reprise De Mission")
                return MissionType::RepriseDeMission;
	if(type == "Demande De Rapport")
                return MissionType::DemandeDeRapport;
	if(type == "Deminage")
                return MissionType::Deminage;
	if(type == "Mesures")
		return MissionType::Mesure;
	else 
		return MissionType::Autre;


}

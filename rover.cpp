#include "rover.h"

Rover::Rover(const std::string& vip, int vport, const std::string& vname, const std::string& vtype, int vid)
    :ip(vip), port(vport), name(vname), type(vtype)
{
	if(vid != -1)
                id = vid;
        else
                id = Rover::idRover;


        if(vid>Rover::idRover)
            Rover::idRover=vid+1;
        else
                Rover::idRover++;
}

Rover::~Rover()
{
    std::cout << "Destruction du rover " << id << std::endl;
}

int Rover::getId() const
{
    return id;
}

std::string Rover::getIp() const
{
    return ip;
}

int Rover::getPort() const
{
    return port;
}

std::string Rover::getName() const
{
    return name;
}

std::string Rover::getType() const
{
    return type;
}

int Rover::idRover = 0;


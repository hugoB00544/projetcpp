#ifndef POSTEDECOMMANDEMENT_H
#define POSTEDECOMMANDEMENT_H

#include <iostream>
#include <map>
#include <sqlite3.h>
#include <algorithm>
#include "mission.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <chrono>
#include "rover.h"
#include <thread>
#include "posteDePilotage.h"
#include "base64.h"
#include <fstream>
#include <iostream>

class PosteDeCommandement
{
private:
    std::map<int, PosteDePilotage*> pilotages;
    sqlite3* db; // Pointeur vers la base de données SQLite
	vector<Mission*> missions;
	void AnalyseDuJson(string);
public:
    PosteDeCommandement();
    ~PosteDeCommandement();
    void DemarrageServer();
    void ajoutPilotage(PosteDePilotage*);
    auto getPilotages(){return pilotages;};
    void envoyerOrdre(Mission& mission);
    void enregistrerOrdreMission(const Mission& mission);
    bool robotAEffectue10Missions(int robotId);
    void effectuerRevision(int robotId);
    void initDB();
    vector<Mission*> getMissions(){return missions;};
    void addMission(Mission*);

    bool haveMissionById(int);
    Mission* getMissionById(int);
	void ajoutRapportBdd(Mission*,int,string, string);
	void ajoutImageBdd(int,string,string);
	void ajoutLogs(string);
	void updateMineEtat(Mission*,int);
};


MissionType getMissionTypeFromString(string);
void transcrireJSONEnFichierImage(const string&, const std::string&);
std::vector<unsigned char> base64ToBytes(const std::string&);
#endif // POSTEDECOMMANDEMENT_H


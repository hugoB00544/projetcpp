#ifndef POSTEDEPILOTAGE_H
#define POSTEDEPILOTAGE_H

#include <iostream>
#include <map>
#include "rover.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <chrono>
#include <thread>

using namespace std;

class PosteDePilotage
{
private:
    Rover* robot; // robot avec son ID et son adresse IP
    int id;
    std::string ip;
    int port; // Port du serveur
    std::string name;
    std::string type;
    static int idPilotage;

public:
    PosteDePilotage(std::string vip,int vport,std::string vname,std::string vtype, int vid = -1):ip(vip),port(vport),name(vname),type(vtype)
    {
	if(vid != -1)
		id = vid;
	else
		id = PosteDePilotage::idPilotage;


	if(vid>PosteDePilotage::idPilotage)
	    PosteDePilotage::idPilotage=vid+1;
	else
		PosteDePilotage::idPilotage++;
    };

	int getId(){return id;};
	std::string getIp(){return ip;};
	int getPort(){return port;};
	std::string getName(){return name;};
	std::string getType(){return type;};
	void setRover(Rover* r){robot = r;};
	Rover* getRover(){return robot;};

};

#endif // POSTEDEPILOTAGE_H

